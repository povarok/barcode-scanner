import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  formSuccessContainer: {
    flex: 1,
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  successText: { fontSize: 28, marginBottom: 15 },
});
