import React from 'react';
import { useSelector } from 'app/store';
import { View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Button, Text } from 'react-native-paper';

import NavigationService from 'app/navigation/NavigationService';
import styles from './styles';

const OrderSendSuccess = () => {
  const theme = useSelector((state) => state.themeReducer.theme);

  const onPress = () => NavigationService.goBack();

  return (
    <View style={styles.formSuccessContainer}>
      <Icon name="check" size={200} color={theme.colors.notification} />
      <Text style={styles.successText} theme={theme}>
        Заказ успешно отправлен
      </Text>
      <Button
        color={theme.colors.notification}
        mode={'contained'}
        onPress={onPress}
        theme={theme}>
        <Text>Вернуться к списку заказов</Text>
      </Button>
    </View>
  );
};

export default OrderSendSuccess;
