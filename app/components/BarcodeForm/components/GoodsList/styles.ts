import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  barcodeScrollView: {
    width: '100%',
    padding: 35,
    flexDirection: 'column',
  },
  fab: {
    position: 'absolute',
    left: 30,
    bottom: 30,
  },
  formSuccessContainer: {
    flex: 1,
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  item: { paddingBottom: 150 },
});
