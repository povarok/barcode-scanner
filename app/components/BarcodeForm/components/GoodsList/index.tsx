import React from 'react';
import { ScrollView, View } from 'react-native';

import GoodItem from 'app/components/BarcodeForm/components/GoodItem';
import { OrderItem } from 'app/models/entities';
import styles from './styles';

interface IProps {
  goods: OrderItem[];
}

const GoodsList: React.FC<IProps> = ({ goods }) => {
  return (
    <>
      <ScrollView style={styles.barcodeScrollView}>
        <View style={styles.item}>
          {goods.map((orderItem) => (
            <GoodItem orderItem={orderItem} key={orderItem.id} />
          ))}
        </View>
      </ScrollView>
    </>
  );
};

export default GoodsList;
