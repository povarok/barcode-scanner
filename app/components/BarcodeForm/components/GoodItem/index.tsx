import React from 'react';
import { Image, View } from 'react-native';
import { Button, Text } from 'react-native-paper';

import { useSelector } from 'app/store';
import NavigationService from 'app/navigation/NavigationService';
import { OrderItem } from 'app/models/entities';
import styles from './styles';

interface IProps {
  orderItem: OrderItem;
}

const GoodItem: React.FC<IProps> = ({ orderItem }) => {
  const theme = useSelector((store) => store.themeReducer.theme);

  const uri = __DEV__
    ? orderItem.imageUrl.replace('https://', 'http://')
    : orderItem.imageUrl;

  const onCheckPress = () => {
    //waiting  api endpoint
  };

  const onScanPress = () => {
    NavigationService.navigate('Camera');
  };

  return (
    <View
      style={{
        ...styles.barcodeHolder,
        backgroundColor: theme.colors.surface,
      }}>
      <View style={styles.barcodeTopBar}>
        <View style={styles.topBlock}>
          <Image source={{ uri }} style={styles.img} />
          <View style={styles.itemName}>
            <Text theme={theme} style={styles.name}>
              {orderItem.name}
            </Text>
            <Text theme={theme} style={styles.amount}>
              Количество -{orderItem.quantity}
            </Text>
          </View>
        </View>
        <View style={styles.scanBox}>
          <Button
            disabled={!!orderItem.shouldHaveQrCode}
            mode="outlined"
            style={styles.checked}
            color={
              orderItem.shouldHaveQrCode
                ? theme.colors.error
                : theme.colors.primary
            }
            onPress={onScanPress}>
            {orderItem.shouldHaveQrCode === null
              ? 'Отсканировано'
              : 'Сканировать'}
          </Button>
          <Button
            onPress={onCheckPress}
            style={styles.checked}
            icon={!orderItem.checked ? 'check' : ''}>
            {orderItem.checked ? 'Отмечено' : 'Отметить'}
          </Button>
        </View>
      </View>
    </View>
  );
};

export default GoodItem;
