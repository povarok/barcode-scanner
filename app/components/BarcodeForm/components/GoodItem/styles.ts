import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  topBlock: { flexDirection: 'row', marginBottom: 20 },
  img: { width: 80, height: 80 },
  barcodeTopBar: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    marginTop: 15,
    alignItems: 'center',
    paddingLeft: 10,
    paddingRight: 10,
  },
  barcodeHolder: {
    flexDirection: 'column',
    width: '100%',
    marginBottom: 20,
    borderRadius: 10,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 5,
  },
  name: { fontSize: 18, width: '100%', fontWeight: 'bold' },
  amount: { fontSize: 18, width: '100%' },
  scanBox: { flexDirection: 'row' },
  itemName: { flexDirection: 'column', flex: 1, paddingHorizontal: 10 },
  checked: { flex: 1 },
});

export default styles;
