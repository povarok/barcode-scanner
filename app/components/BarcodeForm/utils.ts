import { Order } from 'app/models/entities';

export const canSendOrder = (order: Order | null) => {
  return order?.goods?.filter((g) => g.scanned === false).length === 0;
};
