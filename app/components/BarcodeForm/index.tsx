import React from 'react';
import { useDispatch } from 'react-redux';
import { Button, Text } from 'react-native-paper';

import { useSelector } from 'app/store';
import { orderActions } from 'app/store/reducers/orderReducer';
import { Order } from 'app/models/entities';
import NavigationService from 'app/navigation/NavigationService';
import OrderSendSuccess from './components/OrderSendSuccess';
import GoodsList from './components/GoodsList';
import { canSendOrder } from './utils';

interface IProps {
  order: Order;
}

const BarcodeForm: React.FC<IProps> = ({ order }) => {
  const dispatch = useDispatch();
  const selectedOrder = useSelector(
    (store) => store.orderReducer.selectedOrder,
  );
  const loading = useSelector((state) => state.orderReducer.loading);
  const theme = useSelector((state) => state.themeReducer.theme);

  const canSend = canSendOrder(selectedOrder);
  const saved = false;

  const onScanPress = () => {
    canSend
      ? dispatch(orderActions.sendOrderRequest())
      : NavigationService.navigate('Camera');
  };

  if (saved) {
    return <OrderSendSuccess />;
  }
  return (
    <>
      <Button
        icon="send"
        theme={theme}
        mode="contained"
        disabled={!canSend}
        loading={loading}
        onPress={onScanPress}>
        <Text>{canSend ? 'Отправить заказ' : 'Сканировать заказ'}</Text>
      </Button>
      <GoodsList goods={order.orderItems} />
    </>
  );
};

export default BarcodeForm;
