import React from 'react';
import { View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { useDispatch, useSelector } from 'react-redux';
import { Switch } from 'react-native-paper';

import { RootState } from 'app/store';
import { themeActions } from 'app/store/reducers/themeReducer';
import styles from './styles';

const ThemeController: React.FC = () => {
  const dispatch = useDispatch();

  const theme = useSelector((state: RootState) => state.themeReducer.theme);
  const isDark = useSelector((state: RootState) => state.themeReducer.isDark);

  const onToggleTheme = () => dispatch(themeActions.toggleTheme());
  const iconName = isDark ? 'weather-night' : 'white-balance-sunny';
  const iconColor = isDark ? 'white' : 'black';

  return (
    <View style={styles.container}>
      <Switch value={isDark} theme={theme} onValueChange={onToggleTheme} />
      <Icon name={iconName} size={20} style={styles.icon} color={iconColor} />
    </View>
  );
};

export default ThemeController;
