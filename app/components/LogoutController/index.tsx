import React from 'react';
import { useDispatch } from 'react-redux';
import { Button } from 'react-native-paper';
import { loginActions } from 'app/store/reducers/loginReducer';

const LogoutController: React.FC = () => {
  const dispatch = useDispatch();
  return (
    <Button
      icon="logout"
      onPress={() => dispatch(loginActions.logout())}
      style={{ justifyContent: 'center', alignItems: 'center' }}>
      logout
    </Button>
  );
};

export default LogoutController;
