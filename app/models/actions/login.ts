export interface IResponse {
  id: number;
  token: string;
}
