export interface ILoginResponse {
  id: number;
  sessionGuid: string;
}
