import { DarkTheme, DefaultTheme } from 'react-native-paper';

export interface IThemeState {
  isDark: boolean;
  theme: typeof DarkTheme | typeof DefaultTheme;
}
