import { Order } from 'app/models/entities';

export interface IOrderState {
  orders: Order[] | null;
  loading: boolean;
  selectedOrder: Order | null;
}
