export type Order = {
  id: number;
  orderSum: number;
  createdAt: string;
  locked: boolean;
  itemsCount: number;
  images: string[];
  orderItems: OrderItem[];
  goods: Good[];
};

export type Good = {
  id: number;
  name?: string;
  amount: number;
  barcode: string;
  datamatrix?: string;
  images?: string[];
  itemsCount: number;
  scanned: boolean;
  barcodeScanned?: number;
  datamatrixScanned?: number;
};

export type OrderItem = {
  barCode: string;
  id: number;
  imageUrl: string;
  name: string;
  price: number;
  quantity: number;
  shouldHaveQrCode: boolean;
  checked: boolean;
};

export type DetailedOrder = {
  createdAt: string;
  id: number;
  orderSum: number;
  orderItems: OrderItem[];
};
