import axios from 'axios';
import ApiConfig from '../config/api-config';
import { store } from '../Entrypoint';
import { ILoginResponse } from 'app/models/api/login';

export const apiClient = axios.create({
  baseURL: ApiConfig.BASE_URL,
  withCredentials: true,
  responseType: 'json',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json; charset=utf-8',
  },
});

apiClient.interceptors.request.use(
  (request) => {
    request.headers['X-Token'] = store.getState().loginReducer.token;
    return request;
  },
  (error) => error,
);

export const login = (login: string, password: string) => {
  return apiClient.post<ILoginResponse>('/auth/login', {
    login,
    password,
  });
};

export const getOrders = () => {
  return apiClient.get('/orders/');
};

export const getOrder = async (orderId: number) => {
  return apiClient.get(`/orders/${orderId}`);
};

export const lockOrder = (orderId: number) => {
  return apiClient.post('/lock_order', { orderId });
};

export const releaseOrder = (orderId: number) => {
  return apiClient.post('/release_order', { orderId });
};

export const sendOrder = async (barcodes: string[], token: string) => {
  return apiClient.post('/save_order', { barcodes, token });
};
