const ApiConfig = {
  BASE_URL: process.env.API_URL,
};

export default ApiConfig;
