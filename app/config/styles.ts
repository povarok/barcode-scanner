const AppStyles = {
  color: {
    COLOR_PRIMARY: '#C70B7B',
    COLOR_SECONDARY: '#111',
    COLOR_WHITE: '#FFFFFF',
    COLOR_BLACK: '#000000',
    COLOR_GREY: 'grey',
    COLOR_GREEN: 'green',
    COLOR_PLACEHOLDER: '#111111',
    COLOR_GREY_WHITE: '#fafafa',
    COLOR_DARK_SEPARATOR: '#d4d4d4',
    COLOR_BLACK_TRANSPARENT: 'rgba(0, 0, 0, 0.7)',
    COLOR_GREY_TRANSPARENT: 'rgba(67, 85, 85, 0.7)',
  },
};
export default AppStyles;
