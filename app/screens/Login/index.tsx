import React, { useState } from 'react';
import { View, Image, Dimensions } from 'react-native';
import { Button, TextInput, Text } from 'react-native-paper';
import { useDispatch, useSelector } from 'react-redux';
import VersionNumber from 'react-native-version-number';

import { RootState } from 'app/store';
import { loginActions } from 'app/store/reducers/loginReducer';
import images from 'app/config/images';
import styles from './styles';

const Login: React.FC = () => {
  const dispatch = useDispatch();

  const theme = useSelector((state: RootState) => state.themeReducer.theme);

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const screenHeight = Dimensions.get('screen').height;

  const fillForm = () => {
    if (!__DEV__) {
      return;
    }
    setUsername('test');
    setPassword('96kema92');
  };
  const onLogin = () => {
    dispatch(loginActions.loginRequest({ username, password }));
  };
  const formInvalid = () => !username.length || !password.length;

  return (
    <View style={styles.container}>
      <View style={styles.form}>
        <Image
          source={images.icons.logo}
          style={{ ...styles.logo, width: 'auto', height: screenHeight / 7 }}
        />
        <TextInput
          theme={theme}
          label="Имя пользователя"
          value={username}
          onChangeText={(username) => setUsername(username)}
          style={styles.textInput}
        />
        <TextInput
          theme={theme}
          label="Пароль"
          secureTextEntry
          value={password}
          onChangeText={(password) => setPassword(password)}
          style={styles.textInput}
        />
        <Button
          theme={theme}
          icon="login"
          mode="outlined"
          onPress={onLogin}
          disabled={formInvalid()}>
          Войти
        </Button>
        {__DEV__ ? (
          <Button theme={theme} icon="login" mode="outlined" onPress={fillForm}>
            Войти test
          </Button>
        ) : null}
      </View>
      <View style={styles.appVersionText}>
        <Text theme={theme}>Версия приложения: {VersionNumber.appVersion}</Text>
      </View>
    </View>
  );
};

export default Login;
