import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    width: '100%',
    paddingLeft: 40,
    paddingRight: 40,
    justifyContent: 'space-between',
  },
  textInput: {
    width: '100%',
    marginTop: 5,
    marginBottom: 30,
  },
  login: {
    padding: 8,
  },
  forgot: {
    marginTop: 12,
  },
  labelStyle: {
    fontSize: 12,
  },
  appVersionText: {
    position: 'absolute',
    bottom: 25,
    right: 15,
  },
  logo: {
    resizeMode: 'stretch',
    aspectRatio: 1,
    marginBottom: 55,
  },
});

export default styles;
