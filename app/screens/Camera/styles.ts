import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black',
  },
  wrapper: { flex: 1, padding: 10 },
  preview: {
    flex: 1,
    borderWidth: 1,
    borderColor: 'red',
    justifyContent: 'flex-end',
    alignItems: 'center',
    position: 'relative',
  },
  inputs: {
    position: 'absolute',
    flex: 1,
    // borderColor: 'red',
    borderWidth: 1,
    borderRadius: 20,
    backgroundColor: 'white',
    alignItems: 'center',
    width: '100%',
    bottom: 0,
    zIndex: 3,
  },
  barcodeBackground: {
    position: 'absolute',
    flex: 1,
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
    opacity: 0.7,
    backgroundColor: 'black',
  },
  barcodeBorder: {
    borderWidth: 2,
    borderColor: 'red',
    opacity: 0.5,
    position: 'absolute',
  },
  barcodesTotalCounter: {
    position: 'absolute',
    top: 0,
    right: 0,
    backgroundColor: 'green',
    width: 30,
    height: 30,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default styles;
