import { Dimensions } from 'react-native';
const screen = Dimensions.get('screen');
const sw = screen.width;
const sh = screen.height;
const regCoe = 0.7;
const leftMargin = sh / 4 - (sh * regCoe) / 4;
const topMargin = sw / 2 - (sw * regCoe) / 2;
const frameWidth = (sh * regCoe) / 2-50;
const frameHeight = sw * regCoe;
const camViewHeight = Math.round(sh / 2);
const camViewWidth = sw;

const cameraConfig = {
  camViewHeight,
  camViewWidth,
  leftMargin,
  topMargin,
  frameWidth,
  frameHeight,
  scanAreaX: leftMargin / camViewHeight - frameWidth / camViewHeight / 2,
  scanAreaY: topMargin / camViewWidth,
  scanAreaWidth: frameWidth / camViewHeight,
  scanAreaHeight: frameHeight / camViewWidth,
};

export default cameraConfig;
