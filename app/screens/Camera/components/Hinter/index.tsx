import { useSelector } from 'app/store';
import { Text, View } from 'react-native';
import React from 'react';

import styles from './styles';

const Hinter = () => {
  const selectedOrder = useSelector(
    (store) => store.orderReducer.selectedOrder,
  );
  const barcodesScanTargets = selectedOrder!.orderItems.map((g) =>
    new Array(g.quantity)
      .fill(g.barCode)
      .map((e, i) => ({ id: g.id, num: i, barcode: e })),
  );
  const datamatrixScanTargets = selectedOrder!.goods
    .filter((g) => g.datamatrix !== null)
    .map((g) =>
      new Array(g.amount)
        .fill(null)
        .map((_, i) => ({ id: g.id, num: i, datamatrix: g.datamatrix })),
    );
  return (
    <View style={styles.main}>
      <Text>
        Осталось {barcodesScanTargets.length} товаров, для которых требуется
        сканирование штрих-кода
      </Text>
      <Text>
        Осталось {datamatrixScanTargets.length} товаров, для которых требуется
        сканирование QR
      </Text>
    </View>
  );
};

export default Hinter;
