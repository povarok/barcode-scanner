import React, { useRef, useState, useEffect } from 'react';
import { BarCodeReadEvent, RNCamera } from 'react-native-camera';
import { View } from 'react-native';

import config from './config';
import { Good } from 'app/models/entities';
import { store } from '../../Entrypoint';
import {
  compareBarcode,
  compareDatamatrix,
  needScanBarcode,
  needScanDatamatrix,
} from 'app/screens/Camera/utils';
import Hinter from './components/Hinter';
import styles from './styles';

const barcodeInitialState: Good = {
  amount: 0,
  scanned: false,
  id: 0,
  barcode: '',
  datamatrix: '',
  name: '',
  itemsCount: 0,
};

const Camera: React.FC = () => {
  const cameraRef = useRef<RNCamera>(null);

  const [barcode, setBarcode] = useState(barcodeInitialState);

  useEffect(() => {
    if (barcode.barcode && barcode.datamatrix) {
      setBarcode(barcodeInitialState);
    }
  }, [barcode]);

  const { camViewHeight, camViewWidth } = config;

  const onBarCodeRead = (event: BarCodeReadEvent) => {
    const targetGood = store
      .getState()
      .orderReducer.selectedOrder!.goods.find(
        (g) => needScanBarcode(g) || needScanDatamatrix(g),
      );
    if (targetGood === undefined) {
      return;
    }
    if (needScanBarcode(targetGood)) {
      compareBarcode(event, targetGood);
      return;
    }
    if (needScanDatamatrix(targetGood)) {
      compareDatamatrix(event, targetGood);
      return;
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.wrapper}>
        <RNCamera
          ref={cameraRef}
          style={styles.preview}
          onBarCodeRead={onBarCodeRead}
          captureAudio={false}
          autoFocus={'on'}
          cameraViewDimensions={{
            width: camViewWidth,
            height: camViewHeight,
          }}
          type={RNCamera.Constants.Type.back}
        />
      </View>
      <Hinter />
    </View>
  );
};

export default Camera;
