import { Good } from 'app/models/entities';
import { store } from 'app/Entrypoint';
import { orderActions } from 'app/store/reducers/orderReducer';
import { BarCodeReadEvent } from 'react-native-camera';

export const compareBarcode = (event: any, good: Good) => {
  if (event.type === 'EAN_13' && event.data === good.barcode) {
    store.dispatch(
      orderActions.setGoodBarcodeScanned({ goodId: good.id, scanned: true }),
    );
  } else {
    console.warn('skip update barcode ');
  }
};

export const compareDatamatrix = (event: BarCodeReadEvent, good: Good) => {
  if (event.type === 'datamatrix' && event.data === good.datamatrix) {
    store.dispatch(
      orderActions.setGoodDatamatrixScanned({ goodId: good.id, scanned: true }),
    );
  }
};

export const needScanBarcode = (g: Good) => {
  return g.barcodeScanned === undefined || g.barcodeScanned < g.amount;
};

export const needScanDatamatrix = (g: Good) => {
  return (
    g.datamatrix !== undefined &&
    (g.datamatrixScanned === undefined || g.datamatrixScanned < g.amount)
  );
};
