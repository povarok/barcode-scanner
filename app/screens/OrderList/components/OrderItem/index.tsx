import React from 'react';
import { Image, TouchableOpacity, View } from 'react-native';
import { Text } from 'react-native-paper';

import { useSelector } from 'app/store';
import NavigationService from 'app/navigation/NavigationService';
import { formatGoodsString } from 'app/utils/stringUtils';
import { Order } from 'app/models/entities';
import styles from './styles';

interface IProps {
  order: Order;
}

const OrderItem: React.FC<IProps> = ({ order }) => {
  const theme = useSelector((state) => state.themeReducer.theme);

  const onPress = () => {
    NavigationService.navigate('Order', { orderId: order.id });
  };

  return (
    <TouchableOpacity
      style={[styles.card, { backgroundColor: theme.colors.surface }]}
      onPress={onPress}>
      <View style={styles.cardContent}>
        <View style={styles.cardContentText}>
          <Text style={{ fontSize: 20 }} theme={theme}>
            #{order.id}
          </Text>
          <Text style={{ fontSize: 20 }} theme={theme}>
            {order.orderSum}
          </Text>
          <Text style={{ fontSize: 20 }} theme={theme}>
            {order.createdAt}
          </Text>
          <Text style={{ fontSize: 20 }} theme={theme}>
            {order.locked}
          </Text>
          <Text style={{ fontSize: 20 }} adjustsFontSizeToFit theme={theme}>
            {formatGoodsString(order.itemsCount)}
          </Text>
        </View>
        <View style={styles.cardImages}>
          {order.images.map((img) => (
            <View key={img} style={styles.img}>
              <Image style={styles.listImage} source={{ uri: img }} />
            </View>
          ))}
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default OrderItem;
