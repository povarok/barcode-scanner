import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  card: {
    borderRadius: 5,
    width: '100%',
    padding: 10,
    marginBottom: 10,
    backgroundColor: 'transparent',
  },
  cardContent: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
  },
  cardContentText: {
    marginRight: 15,
    padding: 10,
  },
  cardImages: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-start',
    alignContent: 'flex-start',
    width: '100%',
  },
  img: {
    width: '100%',
  },
  listImage: {
    width: '100%',
    marginRight: 10,
  },
});

export default styles;
