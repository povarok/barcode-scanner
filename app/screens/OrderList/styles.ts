import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  flatList: {
    width: '100%',
    backgroundColor: 'transparent',
  },
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  listImage: {
    width: 120,
    height: 100,
    resizeMode: 'contain',
    marginRight: 5,
  },
  fab: {
    position: 'absolute',
    right: 30,
    bottom: 30,
  },
  loader: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    marginBottom: 15,
  },
  cardContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  card: {
    borderRadius: 5,
    width: '100%',
    padding: 10,
    marginBottom: 10,
    backgroundColor: 'transparent',
  },
  cardContent: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
  },
  cardContentText: {
    marginRight: 15,
    padding: 10,
  },
  cardImages: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-start',
    alignContent: 'flex-start',
    width: '100%',
  },
  img: {
    width: '100%',
  },
});

export default styles;
