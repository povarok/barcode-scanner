import React, { useEffect } from 'react';
import { View, FlatList, RefreshControl } from 'react-native';
import { ActivityIndicator, Text, Headline } from 'react-native-paper';
import { useDispatch } from 'react-redux';

import { useSelector } from 'app/store';
import OrderItem from './components/OrderItem';
import { orderActions } from 'app/store/reducers/orderReducer';
import styles from './styles';

const OrderList: React.FC = () => {
  const dispatch = useDispatch();

  const { theme } = useSelector((state) => state.themeReducer);
  const orders = useSelector((state) => state.orderReducer.orders);
  const loading = useSelector((state) => state.orderReducer.loading);
  const getOrders = () => dispatch(orderActions.getOrders());

  useEffect(() => {
    getOrders();
  }, []);

  return (
    <View style={styles.container}>
      <Headline style={styles.title}>Заказы</Headline>
      {loading ? (
        <ActivityIndicator style={styles.loader} size={'large'} theme={theme} />
      ) : (
        <FlatList
          refreshControl={
            <RefreshControl
              refreshing={loading}
              onRefresh={() => getOrders()}
            />
          }
          style={styles.flatList}
          data={orders}
          keyExtractor={(order) => String(order.id)}
          renderItem={({ item: order }) => <OrderItem order={order} />}
          ListEmptyComponent={<Text>список заказов пуст</Text>}
        />
      )}
    </View>
  );
};

export default OrderList;
