import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { Text, View } from 'react-native';
import { Headline } from 'react-native-paper';
import { Route, useRoute } from '@react-navigation/native';

import { useSelector } from 'app/store';
import { orderActions } from 'app/store/reducers/orderReducer';
import BarcodeForm from 'app/components/BarcodeForm';
import styles from './styles';

const Order: React.FC = () => {
  const dispatch = useDispatch();
  const route = useRoute<Route<'Order', { orderId: number }>>();
  const orderId = route.params.orderId;

  const selectedOrder = useSelector(
    (store) => store.orderReducer.selectedOrder,
  );
  useEffect(() => {
    dispatch(orderActions.getOrder({ orderId }));
  }, [orderId]);

  return (
    <View style={styles.container}>
      <Headline style={{ marginBottom: 15 }}>Заказ #{orderId}</Headline>
      {selectedOrder === null || selectedOrder.id !== orderId ? (
        <Text>загрузка</Text>
      ) : (
        <BarcodeForm order={selectedOrder} />
      )}
    </View>
  );
};

export default Order;
