export const formatGoodsString = (amount: number) => {
  const n = Math.abs(amount) % 100;
  const n1 = n % 10;

  if (n > 10 && n < 20) {
    return `${amount} товаров`;
  }
  if (n1 > 1 && n1 < 5) {
    return `${amount} товара`;
  }
  if (n1 === 1) {
    return `${amount} товар`;
  }
  return `${amount} товаров`;
};
