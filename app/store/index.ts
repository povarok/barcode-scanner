import { createStore, compose, applyMiddleware } from 'redux';
import { persistStore, persistCombineReducers } from 'redux-persist';
import {
  TypedUseSelectorHook,
  useSelector as useReduxSelector,
} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import createSagaMiddleware from 'redux-saga';
import thunk from 'redux-thunk';

import sagas from 'app/store/sagas';
import { orderReducer } from './reducers/orderReducer';
import { loadingReducer } from './reducers/loadingReducer';
import { loginReducer } from './reducers/loginReducer';
import { themeReducer } from './reducers/themeReducer';

const config = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['themeReducer', 'loginReducer'],
  blacklist: ['loadingReducer'],
  debug: false,
};

const middleware = [];
const sagaMiddleware = createSagaMiddleware();

middleware.push(sagaMiddleware);
middleware.push(thunk);

const reducers = persistCombineReducers(config, {
  themeReducer,
  orderReducer,
  loadingReducer,
  loginReducer,
});

export type RootState = {
  themeReducer: ReturnType<typeof themeReducer>;
  orderReducer: ReturnType<typeof orderReducer>;
  loadingReducer: ReturnType<typeof loadingReducer>;
  loginReducer: ReturnType<typeof loginReducer>;
};

export const useSelector: TypedUseSelectorHook<RootState> = useReduxSelector;

const enhancers = [applyMiddleware(...middleware)];
const persistConfig: any = { enhancers };
const store = createStore(reducers, undefined, compose(...enhancers));
const persistor = persistStore(store, persistConfig);
const configureStore = () => {
  return { persistor, store };
};

sagaMiddleware.run(sagas);

export default configureStore;
