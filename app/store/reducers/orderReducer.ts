import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { IOrderState } from 'app/models/reducers/order';

const initialState: IOrderState = {
  orders: null,
  loading: false,
  selectedOrder: null,
};

const barcodeSlice = createSlice({
  name: 'order',
  initialState,
  reducers: {
    setSelectedOrder: (state, action) => {
      state.selectedOrder = {
        ...action.payload,
        datamatrixScanned: false,
        barcodeScanned: false,
      };
    },
    setOrders: (state, action) => {
      state.orders = action.payload;
      state.loading = false;
    },
    addOrder: (state, action) => {
      state.orders = state.orders
        ? [...state.orders, action.payload]
        : [action.payload];
    },
    getOrders: (state) => {
      state.loading = true;
    },
    sendOrderRequest: (state) => {
      state.loading = true;
    },
    sendOrderSuccess: (state) => {
      state.loading = false;
    },
    sendOrderFail: (state) => {
      state.loading = false;
    },
    setGoodBarcodeScanned: (
      _,
      action: PayloadAction<{ goodId: number; scanned: boolean }>,
    ) => {
      // TODO
      console.error('setGoodBarcodeScanned not implemented');
      console.log(action.payload);
    },
    setGoodDatamatrixScanned: (
      _,
      action: PayloadAction<{ goodId: number; scanned: boolean }>,
    ) => {
      // TODO
      console.error('setGoodDatamatrixScanned not implemented');
      console.log(action.payload);
    },

    // Saga actions
    getOrder: (_, action: PayloadAction<{ orderId: number }>) => {},
  },
});

export const { actions: orderActions, reducer: orderReducer } = barcodeSlice;
