import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { ILoginState } from 'app/models/reducers/login';

const initialState: ILoginState = {
  isLoggedIn: false,
  id: 0,
  username: '',
  password: '',
  token: '',
};

const loginSlice = createSlice({
  name: 'login',
  initialState,
  reducers: {
    loginRequest: (
      state,
      action: PayloadAction<{ username: string; password: string }>,
    ) => {
      state.username = action.payload.username;
      state.password = action.payload.password;
    },
    loginResponse: (state, action: PayloadAction<{ sessionGuid: string }>) => {
      state.token = action.payload.sessionGuid;
      state.isLoggedIn = true;
    },
    loginFailed: (state) => {
      state.isLoggedIn = false;
      state.token = '';
      state.password = '';
    },
    logout: (state) => {
      state.isLoggedIn = false;
      state.token = '';
      state.password = '';
    },
  },
});

export const { actions: loginActions, reducer: loginReducer } = loginSlice;
