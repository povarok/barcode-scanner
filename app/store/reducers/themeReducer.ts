import { IThemeState } from 'app/models/reducers/theme';
import { createSlice } from '@reduxjs/toolkit';
import { DefaultTheme, DarkTheme } from 'react-native-paper';

const initialState: IThemeState = {
  isDark: false,
  theme: DefaultTheme,
};

const themeSlice = createSlice({
  name: 'theme',
  initialState,
  reducers: {
    toggleTheme: (state) => {
      state.isDark = !state.isDark;
      state.theme = state.isDark ? DarkTheme : DefaultTheme;
    },
  },
});

export const { actions: themeActions, reducer: themeReducer } = themeSlice;
