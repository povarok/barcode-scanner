import { ILoadingState } from 'app/models/reducers/loading';
import { createSlice } from '@reduxjs/toolkit';

const initialState: ILoadingState = {
  isLoginLoading: false,
};

const loadingSlice = createSlice({
  name: 'loading',
  initialState,
  reducers: {
    enableLoader: (state) => {
      state.isLoginLoading = true;
    },
    disableLoader: (state) => {
      state.isLoginLoading = false;
    },
  },
});

export const {
  actions: loadingActions,
  reducer: loadingReducer,
} = loadingSlice;
