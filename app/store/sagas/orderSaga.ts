import { put, call, select, takeEvery, all } from 'redux-saga/effects';
import { PayloadAction } from '@reduxjs/toolkit';

import { RootState } from '../index';
import * as api from 'app/services/api';
import { orderActions } from '../reducers/orderReducer';

export function* getOrders() {
  try {
    const orders = yield call(api.getOrders);
    yield put(orderActions.setOrders(orders.data));
  } catch (e) {
    yield put(orderActions.setOrders([]));
  }
}

export function* getOrder({ payload }: PayloadAction<{ orderId: number }>) {
  const resp = yield call(api.getOrder, payload.orderId);
  yield put(orderActions.setSelectedOrder(resp.data));
}

export function* lockOrder({ payload }: PayloadAction<{ orderId: number }>) {
  yield call(api.lockOrder, payload.orderId);
}

export function* releaseOrder({ payload }: PayloadAction<{ orderId: number }>) {
  yield call(api.releaseOrder, payload.orderId);
}

export function* sendOrder() {
  const token = yield select((state: RootState) => state.loginReducer.token);
  const barcodes = yield select((state: RootState) => state.orderReducer);
  const response = yield call(api.sendOrder, barcodes, token);
  if (response.error === undefined) {
    yield put(orderActions.sendOrderSuccess());
  } else {
    yield put(orderActions.sendOrderFail());
  }
}

export default function* orderSaga() {
  yield all([
    takeEvery('order/getOrders', getOrders),
    takeEvery('order/getOrder', getOrder),
    takeEvery('order/sendOrderRequest', sendOrder),
    takeEvery('order/lockOrder', lockOrder),
    takeEvery('order/releaseOrder', releaseOrder),
  ]);
}
