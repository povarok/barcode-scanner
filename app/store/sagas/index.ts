import { all, fork } from 'redux-saga/effects';
import loginSaga from './loginSaga';
import orderSaga from './orderSaga';

export default function* watch() {
  yield all([fork(loginSaga), fork(orderSaga)]);
}
