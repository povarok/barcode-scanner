import { put, call, takeEvery, all, select } from 'redux-saga/effects';
import { Alert } from 'react-native';

import { loginActions } from 'app/store/reducers/loginReducer';
import { loadingActions } from 'app/store/reducers/loadingReducer';
import * as api from 'app/services/api';
import { RootState } from '../index';

export function* login() {
  yield put(loadingActions.enableLoader());
  const { username, password } = yield select(
    (state: RootState) => state.loginReducer,
  );
  let response: any;
  try {
    response = yield call(api.login, username, password);
  } catch (e) {
    response = { success: false };
  }

  if ('data' in response) {
    yield put(loginActions.loginResponse(response.data));
    yield put(loadingActions.disableLoader());
  } else {
    yield put(loginActions.loginFailed());
    yield put(loadingActions.disableLoader());
    setTimeout(() => {
      Alert.alert('Ошибка авторизации', 'Неверные данные для входа');
    }, 200);
  }
}

export default function* loginSaga() {
  yield all([takeEvery('login/loginRequest', login)]);
}
