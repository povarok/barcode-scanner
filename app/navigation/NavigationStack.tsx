import * as React from 'react';
import { NavigationContainer, Theme } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { useSelector } from 'react-redux';
import { Appbar } from 'react-native-paper';

import NavigationService, { navigationRef } from './NavigationService';
import Login from 'app/screens/Login';
import Home from 'app/screens/OrderList';
import ForgotPassword from 'app/screens/ForgotPassword';
import ThemeController from '../components/ThemeController';
import { StatusBar, View } from 'react-native';
import { ILoginState } from 'app/models/reducers/login';
import Camera from '../screens/Camera';
import LogoutController from '../components/LogoutController';
import Order from '../screens/Order';

import { RootState } from '../store';

const Stack = createStackNavigator();
const AuthStack = createStackNavigator();
const LoggedInStack = createStackNavigator();

interface IState {
  loginReducer: ILoginState;
}

interface IProps {
  theme: Theme;
}

export type RootStackParamList = {
  Home: undefined;
  Order: { id: number };
};

const AuthNavigator = () => {
  const isLoggedIn = useSelector(
    (state: IState) => state.loginReducer.isLoggedIn,
  );
  return (
    <AuthStack.Navigator>
      <Stack.Screen
        name="Login"
        component={Login}
        options={{
          title: 'Вход',
          animationTypeForReplace: isLoggedIn ? 'push' : 'pop',
          headerRight: () => <ThemeController />,
        }}
      />
      <Stack.Screen
        name="ForgotPassword"
        component={ForgotPassword}
        options={{
          animationTypeForReplace: isLoggedIn ? 'push' : 'pop',
          headerRight: () => <ThemeController />,
        }}
      />
    </AuthStack.Navigator>
  );
};

const LoggedInNavigator = () => {
  const { theme } = useSelector((state: RootState) => state.themeReducer);

  const HeaderRight = () => (
    <View
      style={{
        flexDirection: 'row',
        alignContent: 'center',
        justifyContent: 'center',
      }}>
      <ThemeController />
      <LogoutController />
    </View>
  );

  return (
    <LoggedInStack.Navigator>
      <Stack.Screen
        name="Home"
        component={Home}
        options={{
          title: 'Список заказов',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          headerRight: HeaderRight,
        }}
      />
      <Stack.Screen
        name="Order"
        component={Order}
        options={{
          title: 'Заказ',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          headerLeft: () => (
            <Appbar.BackAction
              theme={theme}
              color={theme.colors.text}
              onPress={() => NavigationService.goBack()}
            />
          ),
          headerRight: HeaderRight,
        }}
      />
      <Stack.Screen
        name="Camera"
        component={Camera}
        options={{ title: 'Camera' }}
      />
    </LoggedInStack.Navigator>
  );
};

const App: React.FC<IProps> = (props) => {
  const { theme } = props;
  const isLoggedIn = useSelector(
    (state: IState) => state.loginReducer.isLoggedIn,
  );

  return (
    <NavigationContainer ref={navigationRef} theme={theme}>
      <StatusBar barStyle={theme.dark ? 'light-content' : 'dark-content'} />
      <Stack.Navigator>
        {isLoggedIn ? (
          <>
            <Stack.Screen
              name="Home"
              component={LoggedInNavigator}
              options={{
                headerShown: false,
              }}
            />
          </>
        ) : (
          <Stack.Screen
            name="Login"
            component={AuthNavigator}
            options={{
              headerShown: false,
            }}
          />
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
